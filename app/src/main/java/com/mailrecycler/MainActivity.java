package com.mailrecycler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
private RecyclerView mRecyclerView;
private RecyclerView.LayoutManager mlayoutManager;
private List<Mail> MailList;
private MailAdapter mailAdapter;
String[] name ={"Sulthon","SMKN4BDG","Yamaha","Dicoding","Oracle"};
String[] judul ={"invitaion","Pengumuman","Undian","Beasiswa","sign up"};
String[] isi ={"anda mendapatkan undangan ulang tahun","Selamat atas Kelulusan anda",
        "Selamat, anda berhak mendapatkan satu unit Yamaha R6 dari undian","Anda dapat menikmati content pembelajaran di Dicoding",
        "data sign up anda sudah terdaftar kedalam Oracle.com"};
String[] pic ={"S","S","Y","D","O"};
String[] jam ={"10:45am","13:50pm","12:60pm","17:20pm","18:00pm"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recycler);

    if (mRecyclerView != null){
        mRecyclerView.setHasFixedSize(true);
}

        mlayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mlayoutManager);

        MailList = new ArrayList<>();

        for (int i = 0; i < name.length; i++) {
            Mail mail = new Mail(pic[i], name[i], judul[i], isi[i], jam[i]);
            MailList.add(mail);
        }
        mailAdapter = new MailAdapter(MailList);

        mRecyclerView.setAdapter(mailAdapter);
        mailAdapter.notifyDataSetChanged();


    }
}
