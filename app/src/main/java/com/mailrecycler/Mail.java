package com.mailrecycler;

public class Mail {
    private String pic, name, judul, isi, jam;

    public Mail(String pic, String name, String judul, String isi, String jam){
        this.pic = pic;
        this.name = name;
        this.judul = judul;
        this.isi = isi;
        this.jam = jam;
    }

    public String getName() { return name; }
    public void setName(String name){this.name = name;}
    public String getPic() { return pic; }
    public void setPic(String pic){this.pic = pic;}
    public String getJudul() { return judul; }
    public void setJudul(String judul){this.judul = judul;}
    public String getIsi() { return isi; }
    public void setIsi(String isi){this.isi = isi;}
    public String getJam() { return jam; }
    public void setJam(String jam){this.jam = jam;}

}
