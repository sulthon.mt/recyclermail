package com.mailrecycler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MailAdapter extends RecyclerView.Adapter<MailAdapter.MailViewHolder> {

private List<Mail> mailList;

    public static class MailViewHolder extends RecyclerView.ViewHolder {
        public TextView pic,nama,judul,jam,isi;

        public MailViewHolder(View v){
            super(v);
            pic = v.findViewById(R.id.pic);
            nama = v.findViewById(R.id.name);
            judul = v.findViewById(R.id.judul);
            jam = v.findViewById(R.id.jam);
            isi = v.findViewById(R.id.isi);
        }

    }
    public MailAdapter(List<Mail> mailList) { this.mailList = mailList; }

    @Override
    public MailViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View vx = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_list,viewGroup,
                false);

MailViewHolder vh =new MailViewHolder(vx);
return vh;
    }

    @Override
    public void onBindViewHolder( MailViewHolder holder, int position) {
        Mail mail = mailList.get(position);

        holder.nama.setText(String.valueOf(mail.getName()));
        holder.pic.setText(String.valueOf(mail.getPic()));
        holder.judul.setText(String.valueOf(mail.getJudul()));
        holder.jam.setText(String.valueOf(mail.getJam()));
        holder.isi.setText((String.valueOf(mail.getIsi())));
    }

    @Override
    public int getItemCount() {
        return mailList.size();
    }






}
